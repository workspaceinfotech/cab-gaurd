//
//  TeamManagementViewController.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/25/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeamResponse.h"
#import "Team.h"
#import "Response.h"
#import "JoinTeamResponse.h"
#import "JoinTeam.h"

@interface TeamManagementViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSUserDefaults *defaults;
    NSString *baseurl;
}

@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet UIButton *request;

@property (strong, nonatomic) IBOutlet UITextField *type;
@property (strong, nonatomic) TeamResponse *data;
@property (strong, nonatomic) JoinTeamResponse *joinrequests;
@property (strong, nonatomic) Response *resp;
@property (strong, nonatomic) IBOutlet UITableView *tableData;
@property (assign, nonatomic) int check;
@property (assign, nonatomic) BOOL join;


@end
