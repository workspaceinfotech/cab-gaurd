//
//  LoginViewController.m
//  Cabguard
//
//  Created by Workspace Infotech on 8/21/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "LoginViewController.h"
#import "JSONHTTPClient.h"
#import "ToastView.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    defaults = [NSUserDefaults standardUserDefaults];
    baseurl = [defaults objectForKey:@"baseurl"];
    
    self.email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleTap];
    
    self.signup.frame = CGRectInset(self.signup.frame, -1.0f, -1.0f);
    self.signup.layer.borderColor = [UIColor whiteColor].CGColor;
    self.signup.layer.borderWidth = 1.0f;
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self.email resignFirstResponder];
    [self.password resignFirstResponder];
    
}

- (IBAction)signIn:(id)sender {
    
    //[self performSegueWithIdentifier:@"dashboard" sender:self];
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@login/authenticate",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@login/authenticate",baseurl] bodyString:[NSString stringWithFormat:@"email=%@&password=%@",self.email.text,self.password.text]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                    
                                       NSLog(@"error :%@",err);

                                       NSError* error = nil;
                                       self.data = [[LoginResponse alloc] initWithDictionary:json error:&error];
                                       
                                       NSLog(@"%@",self.data);
                                       
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       if(self.data.responseStat.status){
                                           
                                           [defaults setValue:[NSString stringWithFormat:@"%d",self.data.responseData.login.user.id] forKey:@"id"];
                                           [defaults setValue:self.data.responseData.login.user.f_name forKey:@"firstName"];
                                           [defaults setValue:self.data.responseData.login.user.l_name forKey:@"lastName"];
                                           [defaults setValue:self.data.responseData.login.user.address forKey:@"address"];
                                           [defaults setValue:self.data.responseData.login.user.created_date forKey:@"user_created_date"];
                                           
                                           
                                           [defaults setValue:[NSString stringWithFormat:@"%d",self.data.responseData.login.id] forKey:@"login_id"];
                                           [defaults setValue:self.data.responseData.login.email forKey:@"email"];
                                           [defaults setValue:self.data.responseData.login.access_token forKey:@"access_token"];
                                           [defaults setValue:self.data.responseData.login.created_date forKey:@"login_created_date"];
                                           [defaults setValue:[NSString stringWithFormat:@"%d",self.data.responseData.login.type] forKey:@"type"];
                                          
                                          
                                           
                                           [defaults setValue:self.data.responseData.status.name forKey:@"status"];
                                           [self performSegueWithIdentifier:@"dashboard" sender:self];
                                       }
                                       else{
                                           
                                           [ToastView showToastInParentView:self.view withText:self.data.responseStat.msg withDuaration:2.0];
                                       }
                                       
                                       
                                   }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
