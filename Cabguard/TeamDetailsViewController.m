//
//  TeamDetailsViewController.m
//  Cabguard
//
//  Created by Workspace Infotech on 8/26/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "TeamDetailsViewController.h"
#import "TeamManagementViewController.h"
#import "CreateTeamTableViewCell.h"
#import "JSONHTTPClient.h"
#import "User.h"
#import "PasswordLogin.h"
#import "ToastView.h"

@interface TeamDetailsViewController ()

@end

@implementation TeamDetailsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    defaults = [NSUserDefaults standardUserDefaults];
    baseurl = [defaults objectForKey:@"baseurl"];
    self.myObject = [[NSMutableArray alloc] init];
    self.existObject = [[NSMutableArray alloc] init];
    self.teamName.text = [NSString stringWithFormat:@"Name : %@",self.team_name];
    [self getTeam:self.team_id];
    
    if(self.isMember)
    {
        self.joinBtn.hidden = true;
        self.tableData.hidden = false;
        self.leaveBtn.hidden = false;
    }
    else
    {
        self.tableData.hidden = true;
        self.joinBtn.hidden = false;
        self.leaveBtn.hidden = true;
        self.add.hidden=true;

    }
    
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleTap];
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self.type resignFirstResponder];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getTeam:(int) id
{
    NSLog(@"%@",[NSString stringWithFormat:@"%@team/my/get",baseurl]);
    
    self.myObject = [[NSMutableArray alloc] init];
    self.existObject = [[NSMutableArray alloc] init];
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@team/details",baseurl] bodyString:[NSString stringWithFormat:@"team_id=%d",id]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.data = [[CreateResponse alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       
                                       if(self.data.responseStat.status){
                                           
                                           [self.tableData reloadData];
                                           
                                           
                                           NSString *id = [defaults objectForKey:@"id"];
                                           
                                           if([id integerValue] == self.data.responseData.createdBy.id )
                                           {
                                               self.admin = true;
                                               self.add.hidden = NO;
                                               
                                           }
                                           else
                                           {
                                               self.admin = false;
                                               self.add.hidden = YES;
                                           }
                                           
                                           self.teamLead.text = [NSString stringWithFormat:@"Team lead : %@ %@",self.data.responseData.createdBy.f_name,self.data.responseData.createdBy.l_name];
                                       }
                                       
                                       
                                       
                                   }];

    
}

- (void) confirmCreate: (int) id members: (NSString*) members{
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@team/member/add",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@team/member/add",baseurl] bodyString:[NSString stringWithFormat:@"team_id=%d&members=%@",id,members]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.valid = [[Response alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       if(self.valid.responseStat.status)
                                       {
                                           self.add.hidden = NO;
                                           self.confirm.hidden = YES;
                                           self.cancel.hidden = YES;
                                           self.check = false;
                                           
                                           [self getTeam:self.team_id];
                                           [ToastView showToastInParentView:self.view withText:self.valid.responseStat.msg withDuaration:2.0];
                                       }
                                       else
                                       {
                                           [ToastView showToastInParentView:self.view withText:self.valid.responseStat.msg withDuaration:2.0];
                                       }
                                       
                                       
                                   }];
    
    
    
}

- (void) deleteMember: (int) id members: (int) members{
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@team/member/remove",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@team/member/remove",baseurl] bodyString:[NSString stringWithFormat:@"team_id=%d&members=[%d]",id,members]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.valid = [[Response alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       
                                       if(self.valid.responseStat.status)
                                       {
                                           [self getTeam:self.team_id];
                                           [ToastView showToastInParentView:self.view withText:self.valid.responseStat.msg withDuaration:2.0];
                                       }
                                       else
                                       {
                                           [ToastView showToastInParentView:self.view withText:self.valid.responseStat.msg withDuaration:2.0];
                                       }
                                       
                                   }];
    
    
    
}


- (void) populateSearch: (NSString*) keyword{
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@search/user/foradd/",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@search/user/foradd/",baseurl] bodyString:[NSString stringWithFormat:@"keyword=%@",keyword]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.search = [[SearchAddResponse alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       
                                       if(self.data.responseStat.status){
                                           
                                           [self.tableData reloadData];
                                       
                                       }
                                       
                                       
                                       
                                   }];
    
}

- (IBAction)search:(id)sender {
    
    if (self.check) {
        [self populateSearch:self.type.text];
    }
}


- (IBAction)cancel:(id)sender {
    
    
    self.add.hidden = NO;
    self.confirm.hidden = YES;
    self.cancel.hidden = YES;
    self.check = false;
    
    self.existObject = [[NSMutableArray alloc] init];
    [self.tableData reloadData];



}

- (IBAction)confirm:(id)sender {
    
    
    NSString *members=@"[";
    
    for(int i=0;i<self.myObject.count;i++)
    {
        PasswordLogin *check = [self.myObject objectAtIndex:i];
        members = (i==self.myObject.count-1)?[members stringByAppendingString:[NSString stringWithFormat:@"%d]",check.user.id]]:[members stringByAppendingString:[NSString stringWithFormat:@"%d,",check.user.id]];
        
    }
    
    NSLog(@"%@",members);
    
    [self confirmCreate:self.team_id members:members];
    
    
}

- (IBAction)join:(id)sender {
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@join_team/request/send",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@join_team/request/send",baseurl] bodyString:[NSString stringWithFormat:@"team_id=%d",self.team_id]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.join = [[Response alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       
                                       [ToastView showToastInParentView:self.view withText:self.join.responseStat.msg withDuaration:2.0];
                                       
                                       
                                   }];

    
}

- (IBAction)leave:(id)sender {
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@team/member/leave",baseurl] bodyString:[NSString stringWithFormat:@"team_id=%d",self.team_id]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.leave = [[Response alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       
                                       if(self.leave.responseStat.status){
                                           [self getTeam:self.team_id];
                                           self.isMember = false;
                                           self.leaveBtn.hidden = true;
                                           self.joinBtn.hidden= false;
                                           self.tableData.hidden=true;
                                           self.add.hidden=true;
                                           [ToastView showToastInParentView:self.view withText:self.leave.responseStat.msg withDuaration:2.0];
                                       }
                                       else
                                       {
                                           [ToastView showToastInParentView:self.view withText:self.leave.responseStat.msg withDuaration:2.0];
                                       }
    
    
       }];
}



- (IBAction)newMember:(id)sender {
    
    self.myObject = [[NSMutableArray alloc] init];
    
    self.add.hidden = YES;
    self.confirm.hidden = NO;
    self.cancel.hidden = NO;
    self.check = true;
    [self populateSearch:@""];
    
}

#pragma mark - table methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.check)
    {
        return self.search.responseData.count;
    }
    else
    {
        return self.data.responseData.members.count;
    }
    
   
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.check)
    {
        CreateTeamTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        PasswordLogin *data = self.search.responseData[indexPath.row];
        cell.name.text = [NSString stringWithFormat:@"%@ %@",data.user.f_name,data.user.l_name];
        cell.email.text = [NSString stringWithFormat:@"%@",data.email];
        
        cell.button.hidden = false;
        cell.email.hidden = false;
        cell.buttonImage.hidden = false;
        cell.button.tag = indexPath.row;
        [cell.button addTarget:self action:@selector(addMember:) forControlEvents:UIControlEventTouchUpInside];
        cell.sno.text = [NSString stringWithFormat:@"%ld",(long)indexPath.row+1];
        cell.sno.backgroundColor =  [UIColor blackColor];
        cell.sno.layer.cornerRadius = 25.0;
        [cell.sno.layer setMasksToBounds:YES];
        
        for(int i=0;i<self.myObject.count;i++)
        {
            PasswordLogin *check = [self.myObject objectAtIndex:i];
            if(check.user.id == data.user.id)
            {
                cell.button.hidden = true;
                cell.buttonImage.hidden = true;
            }
        }
        
        for(int i=0;i<self.myObject.count;i++)
        {
            PasswordLogin *check = [self.myObject objectAtIndex:i];
            if(check.user.id == data.user.id)
            {
                cell.button.hidden = true;
                cell.buttonImage.hidden = true;
            }
        }
        
        for(int i=0;i<self.existObject.count;i++)
        {
            TeamMember *check = [self.existObject objectAtIndex:i];
            if(check.id == data.user.id)
            {
                cell.button.hidden = true;
                cell.buttonImage.hidden = true;
            }
        }
        
        
        return cell;
    }
    else
    {
        CreateTeamTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        TeamMember *data = self.data.responseData.members[indexPath.row];
        cell.name.text = [NSString stringWithFormat:@"%@ %@",data.f_name,data.l_name];
        cell.sno.text = [NSString stringWithFormat:@"%ld",(long)indexPath.row+1];
        
        
        cell.button.hidden = true;
        cell.buttonImage.hidden=true;
        cell.email.hidden = true;
        cell.sno.layer.cornerRadius = 25.0;
        [cell.sno.layer setMasksToBounds:YES];
        

        [self.existObject addObject:data];
        
        return cell;

    }
    
    
}

-(void)addMember:(UIButton*)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    PasswordLogin *data = self.search.responseData[sender.tag];
    [self.myObject addObject:data];
    CreateTeamTableViewCell *cell = (CreateTeamTableViewCell *)[self.tableData cellForRowAtIndexPath:indexPath];
    cell.button.hidden = true;
}


-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (!self.check && self.admin) {
        
        NSLog(@"Delete");
        TeamMember *data = self.data.responseData.members[indexPath.row];
        
        
        UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:[NSString stringWithFormat:@"Delete"]  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            
            NSLog(@"Delete");
            [self deleteMember:self.team_id members:data.id];
            
        }];
        
        deleteAction.backgroundColor = [UIColor colorWithRed:185.0/255.0 green:97.0/255.0 blue:125.0/255.0 alpha:1];
        
        return @[deleteAction];
    }
    else
    {
        
        UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:[NSString stringWithFormat:@""]  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            
            
        }];
        
        deleteAction.backgroundColor = nil;
        
        return @[deleteAction];
    }
    
    
    
}




-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}







#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"team"]){
        
        TeamManagementViewController *team= segue.destinationViewController;
        team.check = self.checkType;
        
    }
}


@end
