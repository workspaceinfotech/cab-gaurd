//
//  CreateTeamTableViewCell.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/24/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateTeamTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextField *name;
@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet UILabel *sno;
@property (strong, nonatomic) IBOutlet UIImageView *buttonImage;
@property (strong, nonatomic) IBOutlet UITextField *email;

@end
