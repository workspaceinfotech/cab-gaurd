//
//  JoinTeamResponse.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/27/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"
#import "ResponseStatus.h"
#import "JoinTeam.h"

@interface JoinTeamResponse : JSONModel

@property (strong, nonatomic) ResponseStatus  *responseStat;
@property (strong, nonatomic) NSArray <JoinTeam>  *responseData;

@end
