//
//  TeamManagementViewController.m
//  Cabguard
//
//  Created by Workspace Infotech on 8/25/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "TeamManagementViewController.h"
#import "TeamDetailsViewController.h"
#import "CreateTeamTableViewCell.h"
#import "JSONHTTPClient.h"
#import "ToastView.h"

@interface TeamManagementViewController ()

@end

@implementation TeamManagementViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    defaults = [NSUserDefaults standardUserDefaults];
    baseurl = [defaults objectForKey:@"baseurl"];
    
    if (self.check==0) {
        
         self.join=false;
        self.request.hidden = false;
        
    }
    else if(self.check == 1)
    {
        self.request.hidden = true;
    }
    else
    {
       self.request.hidden = true;
    }
    
    [self getTeam];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleTap];
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self.type resignFirstResponder];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getTeam{
    
    
    if(self.check == 0)
    {
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@team/my/get",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@team/my/get",baseurl] bodyString:@""
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.data = [[TeamResponse alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       
                                       if(self.data.responseStat.status){
                                           
                                           [self.tableData reloadData];
                                       }
                                       
                                       
                                       
                                   }];
    }
    else if(self.check == 1)
    {
    NSLog(@"%@",[NSString stringWithFormat:@"%@team/get",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@team/get",baseurl] bodyString:@""
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.data = [[TeamResponse alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       
                                       if(self.data.responseStat.status){
                                           
                                           [self.tableData reloadData];
                                       }
                                       
                                       
                                       
                                   }];
    }
    else
    {
        NSLog(@"%@",[NSString stringWithFormat:@"%@search/team/forjoinrequest",baseurl]);
        
        [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@search/team/forjoinrequest",baseurl] bodyString:[NSString stringWithFormat:@"keyword="]
                                       completion:^(NSDictionary *json, JSONModelError *err) {
                                           
                                           NSLog(@"%@",err);
                                           
                                           NSError* error = nil;
                                           self.data = [[TeamResponse alloc] initWithDictionary:json error:&error];
                                           
                                           if(error)
                                           {
                                               [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                           }
                                           
                                           
                                           if(self.data.responseStat.status){
                                               
                                               [self.tableData reloadData];
                                           }
                                           
                                           
                                           
                                       }];
    }
    
}

- (void) getJoinTeamRequests:(NSString*) keyword{
    
    
        
        NSLog(@"%@",[NSString stringWithFormat:@"%@search/user/request/my/jointeam",baseurl]);
        
        [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@search/user/request/my/jointeam",baseurl] bodyString:[NSString stringWithFormat:@"keyword=%@",keyword]
                                       completion:^(NSDictionary *json, JSONModelError *err) {
                                           
                                           NSLog(@"%@",err);
                                           
                                           NSError* error = nil;
                                           self.joinrequests = [[JoinTeamResponse alloc] initWithDictionary:json error:&error];
                                           
                                           if(error)
                                           {
                                               [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                           }
                                           
                                           
                                           if(self.joinrequests.responseStat.status){
                                               
                                               [self.tableData reloadData];
                                           }
                                           
                                           
                                           
                                       }];
    }


- (IBAction)joinRequests:(id)sender {
    
    if(self.join)
    {
        [self.button setTitle:@"Team List" forState:UIControlStateNormal];
        [self getTeam];
        self.join=false;
    }
    else
    {
        [self.button setTitle:@"Requests" forState:UIControlStateNormal];
        [self getJoinTeamRequests:@""];
        self.join=true;
    }
}


- (IBAction)search:(id)sender {
    
    [self populateSearch:self.type.text];
}


- (void) populateSearch: (NSString*) keyword{
    
    if(self.check == 0)
    {
    NSLog(@"%@",[NSString stringWithFormat:@"%@search/team/formanagement/",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@search/team/formanagement/",baseurl] bodyString:[NSString stringWithFormat:@"keyword=%@",keyword]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.data = [[TeamResponse alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       
                                       if(self.data.responseStat.status){
                                           
                                           [self.tableData reloadData];
                                       }
                                       
                                       
                                       
                                   }];
    }
    else if (self.check == 1)
    {
        NSLog(@"%@",[NSString stringWithFormat:@"%@search/team/formanagement/",baseurl]);
        
        [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@search/team/formanagement/",baseurl] bodyString:[NSString stringWithFormat:@"keyword=%@",keyword]
                                       completion:^(NSDictionary *json, JSONModelError *err) {
                                           
                                           NSLog(@"%@",err);
                                           
                                           NSError* error = nil;
                                           self.data = [[TeamResponse alloc] initWithDictionary:json error:&error];
                                           
                                           if(error)
                                           {
                                               [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                           }
                                           
                                           
                                           if(self.data.responseStat.status){
                                               
                                               [self.tableData reloadData];
                                           }
                                           
                                           
                                           
                                       }];
    }
    else{
        
        NSLog(@"%@",[NSString stringWithFormat:@"%@search/team/forjoinrequest",baseurl]);
        
        [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@search/team/forjoinrequest",baseurl] bodyString:[NSString stringWithFormat:@"keyword=%@",keyword]
                                       completion:^(NSDictionary *json, JSONModelError *err) {
                                           
                                           NSLog(@"%@",err);
                                           
                                           NSError* error = nil;
                                           self.data = [[TeamResponse alloc] initWithDictionary:json error:&error];
                                           
                                           if(error)
                                           {
                                               [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                           }
                                           
                                           
                                           if(self.data.responseStat.status){
                                               
                                               [self.tableData reloadData];
                                           }
                                           
                                           
                                           
                                       }];

        
    }
    
}


- (void) joinTeam: (int) keyword{
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@join_team/request/send",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@join_team/request/send",baseurl] bodyString:[NSString stringWithFormat:@"team_id=%d",keyword]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.resp = [[Response alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       
                                       [ToastView showToastInParentView:self.view withText:self.resp.responseStat.msg withDuaration:2.0];
                                       
                                       
                                   }];
    
}


- (void) acceptJoinTeam: (int) keyword uid:(int) user{
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@join_team/request/accept",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@join_team/request/accept",baseurl] bodyString:[NSString stringWithFormat:@"team_id=%d&u_id=%d",keyword,user]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.resp = [[Response alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       [self getJoinTeamRequests:@""];
                                       
                                       [ToastView showToastInParentView:self.view withText:self.resp.responseStat.msg withDuaration:2.0];
                                       
                                       
                                   }];
    
}

- (void) rejectJoinTeam: (int) keyword uid:(int) user{
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@join_team/request/reject",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@join_team/request/reject",baseurl] bodyString:[NSString stringWithFormat:@"team_id=%d&u_id=%d",keyword,user]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.resp = [[Response alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       
                                       [self getJoinTeamRequests:@""];

                                       [ToastView showToastInParentView:self.view withText:self.resp.responseStat.msg withDuaration:2.0];
                                       
                                       
                                   }];
    
}

- (void) teamDelete: (int) keyword{
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@team/delete",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@team/delete",baseurl] bodyString:[NSString stringWithFormat:@"team_id=%d",keyword]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.resp = [[Response alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       
                                       if(self.resp.responseStat.status){
                                           
                                           [self getTeam];
                                           [ToastView showToastInParentView:self.view withText:self.resp.responseStat.msg withDuaration:2.0];
                                       }
                                       else
                                       {
                                           [ToastView showToastInParentView:self.view withText:self.resp.responseStat.msg withDuaration:2.0];
                                       }
                                       
                                       
                                       
                                   }];
    
}




#pragma mark - table methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (self.join) {
        return self.joinrequests.responseData.count;
    }
    else
    {
       return self.data.responseData.count;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (self.join) {
        CreateTeamTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        JoinTeam *data = self.joinrequests.responseData[indexPath.row];
        cell.name.text = [NSString stringWithFormat:@"%@ %@ wants to join %@",data.user.f_name,data.user.l_name,data.team.name];
        cell.sno.text = [NSString stringWithFormat:@"%ld",(long)indexPath.row+1];
        
        
        cell.button.hidden = true;
        cell.sno.layer.cornerRadius = 25.0;
        [cell.sno.layer setMasksToBounds:YES];
        
        cell.sno.frame = CGRectInset(cell.sno.frame, -0.5f, -0.5f);
        cell.sno.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.sno.layer.borderWidth = 0.5f;
        
        return cell;
    }
    else
    {
        CreateTeamTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        Team *data = self.data.responseData[indexPath.row];
        cell.name.text = [NSString stringWithFormat:@"%@",data.name];
        cell.sno.text = [NSString stringWithFormat:@"%ld",(long)indexPath.row+1];
        
        
        cell.button.hidden = true;
        cell.sno.layer.cornerRadius = 25.0;
        [cell.sno.layer setMasksToBounds:YES];
        
        cell.sno.frame = CGRectInset(cell.sno.frame, -0.5f, -0.5f);
        cell.sno.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.sno.layer.borderWidth = 0.5f;
        
        return cell;
    }

    
    
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
  
   NSLog(@"Delete");
    
    if(self.check == 0 && self.join==false)
    {
        Team *data = self.data.responseData[indexPath.row];
        
        
        UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:[NSString stringWithFormat:@"Delete"]  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            
            NSLog(@"Delete");
            [self teamDelete:data.id];
            
        }];
        deleteAction.backgroundColor = [UIColor colorWithRed:185.0/255.0 green:97.0/255.0 blue:125.0/255.0 alpha:1];

        
        return @[deleteAction];
    }
    else if(self.check == 0 && self.join==true)
    {
        JoinTeam *data = self.joinrequests.responseData[indexPath.row];
        
        
        UITableViewRowAction *acceptAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:[NSString stringWithFormat:@"Accept"]  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            
            NSLog(@"Accept");
            [self acceptJoinTeam:data.team.id uid:data.user.id];
            
        }];
        acceptAction.backgroundColor = [UIColor colorWithRed:185.0/255.0 green:97.0/255.0 blue:125.0/255.0 alpha:1];
        
        UITableViewRowAction *rejectAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:[NSString stringWithFormat:@"Reject"]  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            
            NSLog(@"Reject");
            [self rejectJoinTeam:data.team.id uid:data.user.id];
            
        }];
        rejectAction.backgroundColor = [UIColor colorWithRed:185.0/255.0 green:97.0/255.0 blue:125.0/255.0 alpha:1];
        
        
        return @[acceptAction,rejectAction];
    }
    else if(self.check == 1)
    {
        
        UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:[NSString stringWithFormat:@""]  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            
                }];
        deleteAction.backgroundColor = Nil;
        
        
        return @[deleteAction];
    }
    else
    {
        Team *data = self.data.responseData[indexPath.row];
        
        
        UITableViewRowAction *joinAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:[NSString stringWithFormat:@"Join"]  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            
            NSLog(@"Delete");
            [self joinTeam:data.id];
            
        }];
        joinAction.backgroundColor = [UIColor colorWithRed:185.0/255.0 green:97.0/255.0 blue:125.0/255.0 alpha:1];
        
        
        return @[joinAction];

    }
}




-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
 
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"teamDetails"]) {
        NSIndexPath *indexPath = [self.tableData indexPathForSelectedRow];
        TeamDetailsViewController *team= segue.destinationViewController;
        Team* data = self.data.responseData[indexPath.row];
        team.team_id = data.id;
        team.team_name = data.name;
        team.checkType = self.check;
        
        if (self.check<=1) {
            team.isMember = true;
        }
        else
        {
            team.isMember = false;
        }
        
        
    }

}


@end
