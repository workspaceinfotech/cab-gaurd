//
//  DashboardViewController.m
//  Cabguard
//
//  Created by Workspace Infotech on 8/21/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "DashboardViewController.h"
#import "TeamManagementViewController.h"
#import "SocketAccess.h"



@interface DashboardViewController ()

@end

@implementation DashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[SocketAccess getSharedInstance]startSocket];
    
    /*if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        [self.teamMng.imageView setImage:[UIImage imageNamed:@"teamMng.png"]];
        [self.teamMy.imageView setImage:[UIImage imageNamed:@"teamMy.png"]];
        [self.teamJoin.imageView setImage:[UIImage imageNamed:@"teamJoin.png"]];
        [self.teamCreate.imageView setImage:[UIImage imageNamed:@"teamCreate.png"]];
        [self.profile.imageView setImage:[UIImage imageNamed:@"profile.png"]];
        [self.password.imageView setImage:[UIImage imageNamed:@"password.png"]];
        [self.gps.imageView setImage:[UIImage imageNamed:@"gps.png"]];
        [self.logout.imageView setImage:[UIImage imageNamed:@"logout.png"]];
       
        
        
    } else if(UIInterfaceOrientationIsLandscape(self.interfaceOrientation)){
        [self.teamMng.imageView setImage:nil];
        [self.teamMy.imageView setImage:nil];
        [self.teamJoin.imageView setImage:nil];
        [self.teamCreate.imageView setImage:nil];
        [self.profile.imageView setImage:nil];
        [self.password.imageView setImage:nil];
        [self.gps.imageView setImage:nil];
        [self.logout.imageView setImage:nil];
    }*/
    
    
    /*[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];*/
    
    
    defaults = [NSUserDefaults standardUserDefaults];
    baseurl = [defaults objectForKey:@"baseurl"];
    
    //self.nameView.frame = CGRectInset(self.nameView.frame, -1.0f, -1.0f);
    //self.nameView.layer.borderColor = [UIColor whiteColor].CGColor;
    //self.nameView.layer.borderWidth = 1.0f;
     
    
    self.name.text = [NSString stringWithFormat:@"%@ %@",[defaults objectForKey:@"firstName"],[defaults objectForKey:@"lastName"]];
    [self.status setTitle:[NSString stringWithFormat:@"%@",[defaults objectForKey:@"status"]] forState:UIControlStateNormal];
    
    
}

/*- (void) orientationChanged:(NSNotification *)note
{
    UIDevice * device = note.object;
    switch(device.orientation)
    {
        case UIDeviceOrientationPortrait:
            [self.teamMng.imageView setImage:[UIImage imageNamed:@"teamMng.png"]];
            [self.teamMy.imageView setImage:[UIImage imageNamed:@"teamMy.png"]];
            [self.teamJoin.imageView setImage:[UIImage imageNamed:@"teamJoin.png"]];
            [self.teamCreate.imageView setImage:[UIImage imageNamed:@"teamCreate.png"]];
            [self.profile.imageView setImage:[UIImage imageNamed:@"profile.png"]];
            [self.password.imageView setImage:[UIImage imageNamed:@"password.png"]];
            [self.gps.imageView setImage:[UIImage imageNamed:@"gps.png"]];
            [self.logout.imageView setImage:[UIImage imageNamed:@"logout.png"]];
            
            

            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            [self.teamMng.imageView setImage:[UIImage imageNamed:@"teamMng.png"]];
            [self.teamMy.imageView setImage:[UIImage imageNamed:@"teamMy.png"]];
            [self.teamJoin.imageView setImage:[UIImage imageNamed:@"teamJoin.png"]];
            [self.teamCreate.imageView setImage:[UIImage imageNamed:@"teamCreate.png"]];
            [self.profile.imageView setImage:[UIImage imageNamed:@"profile.png"]];
            [self.password.imageView setImage:[UIImage imageNamed:@"password.png"]];
            [self.gps.imageView setImage:[UIImage imageNamed:@"gps.png"]];
            [self.logout.imageView setImage:[UIImage imageNamed:@"logout.png"]];
            
            

            break;
        case UIDeviceOrientationLandscapeLeft:
            [self.teamMng.imageView setImage:nil];
            [self.teamMy.imageView setImage:nil];
            [self.teamJoin.imageView setImage:nil];
            [self.teamCreate.imageView setImage:nil];
            [self.profile.imageView setImage:nil];
            [self.password.imageView setImage:nil];
            [self.gps.imageView setImage:nil];
            [self.logout.imageView setImage:nil];
            break;
        case UIDeviceOrientationLandscapeRight:
            [self.teamMng.imageView setImage:nil];
            [self.teamMy.imageView setImage:nil];
            [self.teamJoin.imageView setImage:nil];
            [self.teamCreate.imageView setImage:nil];
            [self.profile.imageView setImage:nil];
            [self.password.imageView setImage:nil];
            [self.gps.imageView setImage:nil];
            [self.logout.imageView setImage:nil];
            break;
            
        default:
            break;
    };
}*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    
     if([segue.identifier isEqualToString:@"adminTeam"]){
        
        TeamManagementViewController *team= segue.destinationViewController;
         team.check = 0;
        
      }
    
     if([segue.identifier isEqualToString:@"myTeam"]){
        
        TeamManagementViewController *team= segue.destinationViewController;
        team.check = 1;
        
    }
    
    if([segue.identifier isEqualToString:@"joinTeam"]){
        
        TeamManagementViewController *team= segue.destinationViewController;
        team.check = 2;
        
    }
}


@end
