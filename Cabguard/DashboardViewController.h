//
//  DashboardViewController.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/21/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterUiButton.h"


@interface DashboardViewController : UIViewController
{
    NSUserDefaults *defaults;
    NSString *baseurl;
}

@property (strong, nonatomic) IBOutlet UIView *nameView;
@property (strong, nonatomic) IBOutlet UITextField *name;
@property (strong, nonatomic) IBOutlet UIButton *status;
@property (strong, nonatomic) IBOutlet CenterUiButton *gps;
@property (strong, nonatomic) IBOutlet CenterUiButton *logout;

@property (strong, nonatomic) IBOutlet CenterUiButton *password;
@property (strong, nonatomic) IBOutlet CenterUiButton *profile;

@property (strong, nonatomic) IBOutlet CenterUiButton *teamMng;
@property (strong, nonatomic) IBOutlet CenterUiButton *teamJoin;
@property (strong, nonatomic) IBOutlet CenterUiButton *teamCreate;
@property (strong, nonatomic) IBOutlet CenterUiButton *teamMy;

@end
