//
//  GPSViewController.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/28/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
/*#import "SocketAuthentication.h"
#import "SocketLogin.h"
#import "Location.h"
#import "SocketResponseStat.h"
#import "SocketTeamData.h"
#import "SocketTeamResponse.h"
#import "User.h"
#import "Team.h"
#import "TeamResponse.h"
#import "SocketResponse.h"
#import "SocketBroadcastData.h"
#import "SocketBroadcastResponse.h"*/
#import "SocketAccess.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@import GoogleMaps;

@interface GPSViewController : UIViewController </*NSStreamDelegate,CLLocationManagerDelegate,*/UITableViewDelegate,UITableViewDataSource>
{
NSUserDefaults *defaults;
NSString *baseurl;
//NSInputStream	*inputStream;
//NSOutputStream	*outputStream;


}

@property (strong, nonatomic) IBOutlet UITableView *tableData;
@property (strong, nonatomic) TeamResponse *data;
@property (strong, nonatomic) IBOutlet UIButton *select;
//@property (strong,nonatomic) SocketAuthentication *responseAuth;
//@property (nonatomic,strong) CLLocationManager *locationManager;

@property (strong, nonatomic) IBOutlet GMSMapView *mapView;

//@property (nonatomic, retain) NSInputStream *inputStream;
//@property (nonatomic, retain) NSOutputStream *outputStream;
//@property (strong, nonatomic) NSString *outputS;
//@property (assign, nonatomic) BOOL receive;
@property (strong, nonatomic) NSTimer *aTimer;



@end
