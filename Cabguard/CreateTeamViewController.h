//
//  CreateTeamViewController.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/24/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchAddResponse.h"
#import "Response.h"
#import "User.h"
#import "CreateResponse.h"

@interface CreateTeamViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSUserDefaults *defaults;
    NSString *baseurl;
}

@property (strong, nonatomic) IBOutlet UIButton *back;
@property (strong, nonatomic) IBOutlet UIButton *next;
@property (strong, nonatomic) IBOutlet UITextField *type;
@property (strong, nonatomic) IBOutlet UILabel *head;
@property (strong, nonatomic) IBOutlet UIButton *search;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *nextSize;

@property (strong, nonatomic)  NSMutableArray *myObject;


@property (strong, nonatomic) IBOutlet UITableView *tableData;
@property (strong, nonatomic) NSString *teamName;
@property (strong, nonatomic) SearchAddResponse *data;
@property (strong, nonatomic) Response *valid;
@property (strong, nonatomic) CreateResponse *create;
@property (assign, nonatomic) int step;





@end
