//
//  TeamResponse.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/25/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"
#import "Team.h"
#import "ResponseStatus.h"

@interface TeamResponse : JSONModel

@property (strong, nonatomic) ResponseStatus  *responseStat;
@property (strong, nonatomic) NSArray <Team>  *responseData;



@end
