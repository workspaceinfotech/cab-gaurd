//
//  ChangePassowordViewController.m
//  Cabguard
//
//  Created by Workspace Infotech on 8/28/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "ChangePassowordViewController.h"
#import "JSONHTTPClient.h"
#import "ToastView.h"

@interface ChangePassowordViewController ()

@end

@implementation ChangePassowordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    defaults = [NSUserDefaults standardUserDefaults];
    baseurl = [defaults objectForKey:@"baseurl"];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleTap];
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self resignFirstResponder];
    
}

- (IBAction)update:(id)sender {
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@profile/change/password",baseurl] bodyString:[NSString stringWithFormat:@"new_password=%@&confirm_password=%@",self.newpass.text,self.confirmpass.text]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.data = [[PasswordResponse alloc] initWithDictionary:json error:&error];
                                       
                                       NSLog(@"%@",self.data);
                                       
                                       
                                       if(self.data.responseStat.status){
                                           
                                           self.newpass.text = @"";
                                           self.confirmpass.text= @"";
                                           [defaults setValue:self.data.responseData.access_token forKey:@"access_token"];
                                           
                                           [ToastView showToastInParentView:self.view withText:@"Password update successful!" withDuaration:2.0];
                                       }
                                       else{
                                           
                                           [ToastView showToastInParentView:self.view withText:self.data.responseStat.msg withDuaration:2.0];
                                       }
                                       
                                       
                                   }];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
