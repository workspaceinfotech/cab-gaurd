//
//  TeamDetailsViewController.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/26/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchAddResponse.h"
#import "Response.h"
#import "CreateResponse.h"
#import "TeamMember.h"

@interface TeamDetailsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSUserDefaults *defaults;
    NSString *baseurl;
}

@property (strong, nonatomic) SearchAddResponse *search;
@property (strong, nonatomic) Response *valid;
@property (strong, nonatomic) Response *leave;
@property (strong, nonatomic) Response *join;
@property (strong, nonatomic) CreateResponse *data;

@property (strong, nonatomic) IBOutlet UITableView *tableData;
@property (strong, nonatomic) IBOutlet UILabel *teamName;
@property (strong, nonatomic) IBOutlet UILabel *teamLead;
@property (strong, nonatomic) IBOutlet UITextField *type;

@property (strong, nonatomic) IBOutlet UIButton *confirm;
@property (strong, nonatomic) IBOutlet UIButton *cancel;
@property (strong, nonatomic) IBOutlet UIButton *add;
@property (strong, nonatomic)  NSMutableArray *myObject;
@property (strong, nonatomic)  NSMutableArray *existObject;
@property (assign, nonatomic) int team_id;
@property (assign, nonatomic) BOOL check;
@property (assign, nonatomic) BOOL isMember;
@property (assign, nonatomic) int checkType;
@property (assign, nonatomic) BOOL admin;
@property (strong, nonatomic) NSString *team_name;
@property (strong, nonatomic) NSString *team_lead;
@property (strong, nonatomic) IBOutlet UIButton *joinBtn;
@property (strong, nonatomic) IBOutlet UIButton *leaveBtn;


@end
