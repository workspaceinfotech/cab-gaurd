//
//  UserStatus.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/21/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"

@interface UserStatus : JSONModel

@property (assign, nonatomic) int id;
//@property (assign, nonatomic) int status_id;
//@property (assign, nonatomic) int login_id;
@property (strong, nonatomic) NSString  *name;
@property (strong, nonatomic) NSString  *created_date;

@end
