//
//  PasswordResponse.h
//  Cabguard
//
//  Created by Workspace Infotech on 9/9/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"
#import "ResponseStatus.h"
#import "PasswordLogin.h"

@interface PasswordResponse : JSONModel

@property (strong, nonatomic) ResponseStatus  *responseStat;
@property (strong, nonatomic) PasswordLogin  *responseData;

@end
