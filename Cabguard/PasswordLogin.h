//
//  PasswordLogin.h
//  Cabguard
//
//  Created by Workspace Infotech on 9/9/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"
#import "User.h"

@protocol PasswordLogin

@end

@interface PasswordLogin : JSONModel

@property (strong, nonatomic) NSString <Optional> *id;
@property (strong, nonatomic) NSString <Optional> *u_id;
@property (strong, nonatomic) NSString <Optional> *email;
@property (strong, nonatomic) NSString <Optional> *access_token;
@property (strong, nonatomic) NSString <Optional> *created_date;
@property (strong, nonatomic) User <Optional> *user;
@property (strong, nonatomic) NSString <Optional> *type;

@end
