//
//  SocketAccess.m
//  Cabguard
//
//  Created by Workspace Infotech on 9/10/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "SocketAccess.h"

static SocketAccess *sharedInstance = nil;

@implementation SocketAccess

@synthesize inputStream, outputStream,mapView,locationManager;

+(SocketAccess*)getSharedInstance{
    
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance initNetworkCommunication];
        [sharedInstance authentication];
    }
    return sharedInstance;
}

+ (void) resetSocket{
    
    sharedInstance = [[super allocWithZone:NULL]init];
    [sharedInstance initNetworkCommunication];
    [sharedInstance authentication];
    
}

- (void) startSocket{
    NSLog(@"Socket Started");
}

-(void) setMap:(GMSMapView *)map{
    self.mapView = map;
}

- (void) authentication
{
    NSError* error;
    
    SocketAuthentication *responseAuth = [[SocketAuthentication alloc]init];
    
    SocketResponseStat *responseStat = [[SocketResponseStat alloc]init];
    
    responseStat.status = true;
    responseStat.command = @"AUTHENTICATION";
    responseStat.onlyReceiverMember = false;
    
    
    SocketLogin *responseData = [[SocketLogin alloc]init];
    responseData.id = [[defaults objectForKey:@"login_id"] intValue];
    responseData.u_id = [[defaults objectForKey:@"id"] intValue];
    responseData.email = [defaults objectForKey:@"email"];
    responseData.access_token = [defaults objectForKey:@"access_token"];
    responseData.created_date = [defaults objectForKey:@"login_created_date"];
    responseData.type = [[defaults objectForKey:@"type"] intValue];
    
    Location *location = [[Location alloc]init];
    location.lat= 10.00;
    location.lon = 10.00;
    
    
    User *user = [[User alloc]init];
    user.id = [[defaults objectForKey:@"id"] intValue];
    user.f_name = [defaults objectForKey:@"f_name"];
    user.l_name = [defaults objectForKey:@"l_name"];
    user.created_date = [defaults objectForKey:@"user_created_date"];
    user.address = [defaults objectForKey:@"address"];
    
    
    responseData.user = user;
    responseData.location = location;
    
    responseAuth.responseStat = responseStat;
    responseAuth.responseData = responseData;
    
   // NSLog(@"%@",self.responseAuth);
    
    
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:[responseAuth toDictionary] options:NSJSONWritingPrettyPrinted error:&error];
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    jsonString=  [[jsonString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
    jsonString = [NSString stringWithFormat:@"%@\n",jsonString];
    NSData *reqdata = [[NSData alloc] initWithData:[jsonString dataUsingEncoding:NSASCIIStringEncoding]];
    
    
   // NSLog(@"%@", jsonString);
    
    [outputStream write:[reqdata bytes] maxLength:[reqdata length]];
    
}

- (void) broadcastLoaction:(NSString*) json
{
    
    NSError* err = nil;
    SocketBroadcastResponse *response = [[SocketBroadcastResponse alloc] initWithString:json error:&err];
    SocketBroadcastResponse *request = [[SocketBroadcastResponse alloc]init];
    SocketBroadcastData *requestData = [[SocketBroadcastData alloc]init];
    SocketResponseStat *responseStat = [[SocketResponseStat alloc]init];
    
    NSError* error;
    
    
    
    responseStat.status = true;
    responseStat.command = @"BROADCAST_LOCATION";
    responseStat.onlyReceiverMember = false;
    
    
    SocketLogin *sender = [[SocketLogin alloc]init];
    sender.id = [[defaults objectForKey:@"login_id"] intValue];
    sender.u_id = [[defaults objectForKey:@"id"] intValue];
    sender.email = [defaults objectForKey:@"email"];
    sender.access_token = [defaults objectForKey:@"access_token"];
    sender.created_date = [defaults objectForKey:@"login_created_date"];
    sender.type = [[defaults objectForKey:@"type"] intValue];
    
    Location *location = [[Location alloc]init];
    location.lat= currentLocation.coordinate.latitude;
    location.lon = currentLocation.coordinate.longitude;
    
    
    NSLog(@"Broadcast Value %f %f %@ %@",location.lat,location.lon,[defaults objectForKey:@"f_name"],[defaults objectForKey:@"l_name"]);
    
    User *user = [[User alloc]init];
    user.id = [[defaults objectForKey:@"id"] intValue];
    user.f_name = [defaults objectForKey:@"firstName"];
    user.l_name = [defaults objectForKey:@"lastName"];
    user.created_date = [defaults objectForKey:@"user_created_date"];
    user.address = [defaults objectForKey:@"address"];
    
    
    sender.user = user;
    sender.location = location;
    
    requestData.sender = sender;
    requestData.receiver = response.responseData.sender;
    
    request.responseStat = responseStat;
    request.responseData = requestData;
    
    
    
    
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:[request toDictionary] options:NSJSONWritingPrettyPrinted error:&error];
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    jsonString=  [[jsonString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
    jsonString = [NSString stringWithFormat:@"%@\n",jsonString];
    NSData *reqdata = [[NSData alloc] initWithData:[jsonString dataUsingEncoding:NSASCIIStringEncoding]];
    
    
   // NSLog(@"%@", jsonString);
    
    [outputStream write:[reqdata bytes] maxLength:[reqdata length]];
    
}

-(void) createTeamMemberLocation:(NSString*) json
{
    GMSMarker *marker =[[GMSMarker alloc] init];
    NSError* err = nil;
    SocketBroadcastResponse *response = [[SocketBroadcastResponse alloc] initWithString:json error:&err];
   // NSLog(@"Create Team member response : %@",response);
    
    if(response)
    {
    
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake(response.responseData.sender.location.lat, response.responseData.sender.location.lon);
        
        //NSLog(@"create location : %f %f",response.responseData.sender.location.lat, response.responseData.sender.location.lat);
        marker.title = response.responseData.sender.user.f_name;
        marker.snippet = response.responseData.sender.user.l_name;
        marker.position = location;
        marker.icon = [UIImage imageNamed:@"marker.png"];
        marker.map = self.mapView;
    }
}

-(UIImage *)imageFromText:(NSString *)text
{
    
    UIFont *font = [UIFont systemFontOfSize:20.0];
    CGSize sizef = [text sizeWithAttributes:
                   @{NSFontAttributeName:
                         [UIFont systemFontOfSize:20.0f]}];
   
    UIGraphicsBeginImageContextWithOptions(sizef,NO,0.0);
    [text drawAtPoint:CGPointMake(0.0, 0.0) withFont:font];
    //[text drawAtPoint:CGPointMake(0.0, 0.0) forWidth:CGPointMake(0, 0) withFont:font fontSize:nil lineBreakMode:NSLineBreakByWordWrapping baselineAdjustment:NSTextAlignmentCenter];
    

    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}



- (void) getTeamMemberLocation:(Team*) team
{
    NSError* error;
    
    SocketTeamResponse *responseAuth = [[SocketTeamResponse alloc]init];
    
    
    
    SocketResponseStat *responseStat = [[SocketResponseStat alloc]init];
    
    responseStat.status = true;
    responseStat.command = @"GET_TEAM_MEMBER_LOCATION";
    responseStat.onlyReceiverMember = false;
    
    
    SocketTeamData *responseData = [[SocketTeamData alloc]init];
    
    responseData.team = team;
    
    
    responseAuth.responseStat = responseStat;
    responseAuth.responseData = responseData;
    
    
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:[responseAuth toDictionary] options:NSJSONWritingPrettyPrinted error:&error];
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    jsonString=  [[jsonString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
    jsonString = [NSString stringWithFormat:@"%@\n",jsonString];
    NSData *reqdata = [[NSData alloc] initWithData:[jsonString dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    //NSLog(@"%@", jsonString);
    
    [outputStream write:[reqdata bytes] maxLength:[reqdata length]];
    
}

-(void) myCurrentLocation
{
    GMSMarker *marker=[[GMSMarker alloc] init];
    marker.title = @"Me";
    marker.snippet = [defaults objectForKey:@"f_name"];
    marker.position = currentLocation.coordinate;
    marker.map = self.mapView;
    
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    
    NSLog(@"stream event %lu", (unsigned long)streamEvent);
    
    switch (streamEvent) {
            
        case NSStreamEventOpenCompleted:
            NSLog(@"Stream opened");
            break;
        case NSStreamEventHasBytesAvailable:
            
            if (theStream == inputStream) {
                
                uint8_t buffer[1024];
                int len;
                
                while ([inputStream hasBytesAvailable]) {
                    NSLog(@"Check Receive :::::::");
                    len = (int)[inputStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0) {
                        
                        
                        
                        NSString *output = [[NSString alloc] initWithBytes:buffer length:len encoding:NSASCIIStringEncoding];
                        NSArray *outputArray = [[NSArray alloc] init];
                        
                        if (nil != output) {
                            
                            //NSLog(@"Check Receive :::::::%@|||",[output substringFromIndex:[output length]-1]);
                           // NSLog(@"server said: %@", output);
                            
                            
                            if([[output substringFromIndex:[output length]-1] isEqualToString:@"\n"])
                            {
                                
                                self.outputS = (self.receive)?output:[NSString stringWithFormat:@"%@%@",self.outputS,output];
                                self.receive = true;
                                
                                //NSLog(@"True Current output: %@",self.outputS);
                                
                                if([[self.outputS componentsSeparatedByString:@"INCOMING_LOCATION"] count]>1)
                                {
                                    
                                    outputArray = [self.outputS componentsSeparatedByString:@"}}}"];
                                    
                                    
                                    for (int i=0;i<[outputArray count];i++) {
                                        
                                        if(i==[outputArray count]-1)
                                        {
                                            
                                            [self createTeamMemberLocation:[outputArray objectAtIndex:i]];
                                            [self myCurrentLocation];
                                            
                                        }
                                        else
                                        {
                                            [self createTeamMemberLocation:[NSString stringWithFormat:@"%@}}}",[outputArray objectAtIndex:i]]];
                                            [self myCurrentLocation];
                                            
                                        }
                                        
                                    }
                                }
                                else
                                {
                                    NSError* err = nil;
                                    SocketResponse *response = [[SocketResponse alloc] initWithString:output error:&err];
                                    //NSLog(@"server said json: %@", err);
                                    //NSLog(@"server said json: %@", response);
                                    
                                    if([response.responseStat.command isEqualToString:@"GIVE_LOCATION"])
                                    {
                                        [self broadcastLoaction:self.outputS];
                                    }
                                    
                                    
                                    
                                    
                                    if([response.responseStat.command isEqualToString:@"INCOMING_LOCATION"])
                                    {
                                        [self createTeamMemberLocation:self.outputS];
                                        [self myCurrentLocation];
                                    }
                                    
                                }
                            }
                            else
                            {
                                self.outputS = [NSString stringWithFormat:@"%@%@",self.outputS,output];
                                self.receive = false;
                                
                                //NSLog(@"False Current output: %@",self.outputS);
                            }
                            
                            
                            
                            
                            
                            
                        }
                    }
                }
            }
            break;
            
            
        case NSStreamEventErrorOccurred:
            
            NSLog(@"Can not connect to the host!");
            break;
            
        case NSStreamEventEndEncountered:
            
            [theStream close];
            [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            theStream = nil;
            
            break;
        default:
            NSLog(@"Unknown event");
    }
    
}




- (void) initNetworkCommunication {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.outputS = @"";
    self.receive = false;
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];
    
    defaults = [NSUserDefaults standardUserDefaults];
    baseurl = [defaults objectForKey:@"baseurl"];

    
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"139.59.166.43", 9091, &readStream, &writeStream);
    
    inputStream = (__bridge NSInputStream *)readStream;
    outputStream = (__bridge NSOutputStream *)writeStream;
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [inputStream open];
    [outputStream open];
    
    
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;
    NSLog(@"%f %f",newLocation.coordinate.longitude,newLocation.coordinate.latitude);
    
    [locationManager stopUpdatingLocation];
}

@end
