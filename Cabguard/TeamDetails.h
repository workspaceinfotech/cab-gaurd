//
//  TeamDetails.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/25/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "Team.h"
#import "User.h"
#import "TeamMember.h"

@interface TeamDetails : Team

@property (strong, nonatomic) User  *createdBy;
@property (strong, nonatomic) NSArray <TeamMember>  *members;



@end
