//
//  RegistrationResponse.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/24/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"
#import "ResponseStatus.h"
#import "ResponseData.h"

@interface RegistrationResponse : JSONModel

@property (strong, nonatomic) ResponseStatus  *responseStat;
@property (strong, nonatomic) ResponseData  *responseData;


@end
