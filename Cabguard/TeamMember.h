//
//  TeamMember.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/25/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "User.h"

@protocol TeamMember

@end

@interface TeamMember : User

@property (assign, nonatomic) BOOL is_lead;
@property (strong, nonatomic) NSString  *join_date;

@end
