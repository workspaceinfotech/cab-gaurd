//
//  JoinTeam.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/27/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"
#import "Team.h"
#import "User.h"

@protocol JoinTeam

@end

@interface JoinTeam : JSONModel

@property (strong, nonatomic) Team  *team;
@property (strong, nonatomic) User  *user;


@end
