//
//  SocketAccess.h
//  Cabguard
//
//  Created by Workspace Infotech on 9/10/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SocketAuthentication.h"
#import "SocketLogin.h"
#import "Location.h"
#import "SocketResponseStat.h"
#import "SocketTeamData.h"
#import "SocketTeamResponse.h"
#import "User.h"
#import "Team.h"
#import "TeamResponse.h"
#import "SocketResponse.h"
#import "SocketBroadcastData.h"
#import "SocketBroadcastResponse.h"

@import GoogleMaps;

@interface SocketAccess : NSObject<NSStreamDelegate,CLLocationManagerDelegate>
{
    NSUserDefaults *defaults;
    NSString *baseurl;
    NSInputStream	*inputStream;
    NSOutputStream	*outputStream;
    CLLocation *currentLocation;
    
}


@property (strong, nonatomic) GMSMapView *mapView;
@property (nonatomic, retain) NSInputStream *inputStream;
@property (nonatomic, retain) NSOutputStream *outputStream;
@property (strong, nonatomic) NSString *outputS;
@property (assign, nonatomic) BOOL receive;
@property (nonatomic,strong) CLLocationManager *locationManager;
@property (strong, nonatomic) TeamResponse *data;
@property (strong,nonatomic) SocketAuthentication *responseAuth;

+(SocketAccess*)getSharedInstance;
+(void)resetSocket;

- (void) getTeamMemberLocation:(Team*) team;
- (void) setMap:(GMSMapView *)map;
- (void) startSocket;
- (void) authentication;
- (void) broadcastLoaction:(NSString*) json;
- (void) createTeamMemberLocation:(NSString*) json;
- (void) myCurrentLocation;
- (void) stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent;
- (void) initNetworkCommunication;


@end
