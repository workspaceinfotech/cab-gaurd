//
//  Login.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/21/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"
#import "User.h"

@protocol Login

@end

@interface Login : JSONModel

@property (assign, nonatomic) int id;
@property (assign, nonatomic) int  u_id;
@property (assign, nonatomic) int  active;
@property (strong, nonatomic) NSString <Optional> *activation_code;
@property (strong, nonatomic) NSString <Optional> *email;
@property (strong, nonatomic) NSString <Optional> *access_token;
@property (strong, nonatomic) NSString <Optional> *created_date;
@property (strong, nonatomic) User <Optional> *user;
@property (assign, nonatomic) int type;

@end
