//
//  SocketTeamData.h
//  Cabguard
//
//  Created by Workspace Infotech on 9/2/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"
#import "Team.h"
#import "User.h"

@interface SocketTeamData : JSONModel

@property (strong,nonatomic) Team *team;
@property (strong,nonatomic) User *receiver;

@end
