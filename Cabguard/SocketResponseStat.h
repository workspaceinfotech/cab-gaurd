//
//  SocketResponseStat.h
//  Cabguard
//
//  Created by Workspace Infotech on 9/2/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"

@interface SocketResponseStat : JSONModel

@property (assign, nonatomic) BOOL  status;
@property (strong, nonatomic) NSString  *command;
@property (assign, nonatomic) BOOL  onlyReceiverMember;

@end
