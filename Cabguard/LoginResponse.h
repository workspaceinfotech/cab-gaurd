//
//  LoginResponse.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/21/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"
#import "LoginResponseData.h"
#import "ResponseStatus.h"

@interface LoginResponse : JSONModel


@property (strong, nonatomic) ResponseStatus  *responseStat;
@property (strong, nonatomic) LoginResponseData  *responseData;

@end
