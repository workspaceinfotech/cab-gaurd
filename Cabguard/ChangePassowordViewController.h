//
//  ChangePassowordViewController.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/28/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PasswordResponse.h"

@interface ChangePassowordViewController : UIViewController
{
    NSUserDefaults *defaults;
    NSString *baseurl;
}

@property (strong, nonatomic) IBOutlet UITextField *newpass;
@property (strong, nonatomic) IBOutlet UITextField *confirmpass;
@property (strong, nonatomic) PasswordResponse *data;



@end
