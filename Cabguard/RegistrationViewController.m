//
//  RegistrationViewController.m
//  Cabguard
//
//  Created by Workspace Infotech on 8/21/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "RegistrationViewController.h"
#import "JSONHTTPClient.h"
#import "ToastView.h"

@interface RegistrationViewController ()

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    defaults = [NSUserDefaults standardUserDefaults];
    baseurl = [defaults objectForKey:@"baseurl"];
    
    self.firstName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"First Name" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.lastName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Last Name" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.address.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Addresss" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.zipcode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Zip code" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleTap];
    
    self.signin.frame = CGRectInset(self.signin.frame, -1.0f, -1.0f);
    self.signin.layer.borderColor = [UIColor whiteColor].CGColor;
    self.signin.layer.borderWidth = 1.0f;
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self.email resignFirstResponder];
    [self.password resignFirstResponder];
    [self.firstName resignFirstResponder];
    [self.lastName resignFirstResponder];
    [self.address resignFirstResponder];
    [self.zipcode resignFirstResponder];
    
}

- (IBAction)signUp:(id)sender {
    NSLog(@"%@",[NSString stringWithFormat:@"%@registration/add/user",baseurl]);
    
    NSString *address = [NSString stringWithFormat:@"%@|%@",self.address.text,self.zipcode.text];
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@registration/add/user",baseurl] bodyString:[NSString stringWithFormat:@"f_name=%@&l_name=%@&address=%@&email=%@&password=%@",self.firstName.text,self.lastName.text,address,self.email.text,self.password.text]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.data = [[RegistrationResponse alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       
                                       if (self.data.responseStat.status) {
                                           [ToastView showToastInParentView:self.view withText:@"Registration successfull!" withDuaration:2.0];
                                           [ToastView showToastInParentView:self.view withText:@"Please Check your mail for confirmation" withDuaration:2.0];
                                           
                                           self.firstName.text = @"";
                                           self.lastName.text = @"";
                                           self.email.text = @"";
                                           self.zipcode.text = @"";
                                           self.address.text = @"";
                                           self.password.text = @"";
                                           
                                       }
                                       else
                                       {
                                           [ToastView showToastInParentView:self.view withText:self.data.responseStat.msg withDuaration:2.0];
                                           
                                       }
                                       
                                       
                                       
                                       
                                   }];
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
