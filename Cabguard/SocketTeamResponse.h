//
//  SocketTeamResponse.h
//  Cabguard
//
//  Created by Workspace Infotech on 9/2/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"
#import "SocketTeamData.h"
#import "SocketResponseStat.h"

@interface SocketTeamResponse : JSONModel

@property (strong, nonatomic) SocketResponseStat *responseStat;
@property (strong, nonatomic) SocketTeamData *responseData;

@end
