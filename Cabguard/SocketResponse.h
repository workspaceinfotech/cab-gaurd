//
//  SocketResponse.h
//  Cabguard
//
//  Created by Workspace Infotech on 9/4/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"
#import "SocketResponseStat.h"

@interface SocketResponse : JSONModel

@property (strong, nonatomic) SocketResponseStat *responseStat;

@end
