//
//  ResponseStatus.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/21/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"

@interface ResponseStatus : JSONModel

@property (assign, nonatomic) BOOL status;
@property (strong, nonatomic) NSString  *msg;

@end
