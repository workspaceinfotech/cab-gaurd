//
//  SocketAuthentication.h
//  Cabguard
//
//  Created by Workspace Infotech on 9/2/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"
#import "SocketResponseStat.h"
#import "SocketLogin.h"

@interface SocketAuthentication : JSONModel

@property (strong, nonatomic) SocketResponseStat *responseStat;
@property (strong, nonatomic) SocketLogin *responseData;

@end
