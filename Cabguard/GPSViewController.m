//
//  GPSViewController.m
//  Cabguard
//
//  Created by Workspace Infotech on 8/28/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "GPSViewController.h"
#import "JSONHTTPClient.h"

@interface GPSViewController ()

@end


@implementation GPSViewController{
    GMSMapView *mapView_;
    BOOL firstLocationUpdate_;
    //CLLocation *currentLocation;
   
}


//@synthesize inputStream, outputStream,mapView,locationManager;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //self.locationManager = [[CLLocationManager alloc] init];
    //self.outputS = @"";
   // self.receive = false;

    //self.locationManager.delegate = self;
    //self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
   // [self.locationManager requestWhenInUseAuthorization];
   // [self.locationManager startUpdatingLocation];

 
    
    
    
   // if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        
    
        
        
   // }
    
    [SocketAccess resetSocket];
    
    self.mapView.myLocationEnabled = YES;
    self.mapView.settings.compassButton = YES;
    self.mapView.settings.zoomGestures = YES;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.accessibilityElementsHidden = NO;
   
    
    
    [self.mapView animateToZoom:1];
    
    
    
    defaults = [NSUserDefaults standardUserDefaults];
    baseurl = [defaults objectForKey:@"baseurl"];
    
    //[self initNetworkCommunication];
    //[self authentication];
    
    [self setPickerData];
    
   
    [[SocketAccess getSharedInstance]setMap:self.mapView];
    
    //[[SocketAccess getSharedInstance]myCurrentLocation];

}

-(void)viewDidDisappear:(BOOL)animated
{
    [self.aTimer invalidate];
    self.aTimer = nil;
}


- (IBAction)selectTeam:(id)sender {
    
    self.mapView.hidden = (self.tableData.hidden)?true:false;
    self.tableData.hidden = (self.tableData.hidden)?false:true;
   
}


#pragma mark - table methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  
  return self.data.responseData.count;
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        Team *data = self.data.responseData[indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"%@",data.name];
        return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Team *data = self.data.responseData[indexPath.row];
    [self.select setTitle:data.name forState:UIControlStateNormal];
    self.tableData.hidden = true;
    self.mapView.hidden = false;
    
    [self.mapView clear];
    
    [[SocketAccess getSharedInstance]getTeamMemberLocation:data];
    self.aTimer = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(handleTimer:) userInfo:data repeats:YES];
    
    
    
}

- (void)handleTimer:(NSTimer*)theTimer {
    
    NSLog(@"Calling Team Member");
    [self.mapView clear];
    [[SocketAccess getSharedInstance]getTeamMemberLocation:[theTimer userInfo]];
    
}





-(void) setPickerData{
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@team/get",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@team/get",baseurl] bodyString:@""
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.data = [[TeamResponse alloc] initWithDictionary:json error:&error];
                                       
                                      
                                       
                                       
                                       if(self.data.responseStat.status){
                                             NSLog(@"%@",self.data);
                                           [self.tableData reloadData];
                                           
                                        }
                                       
                                       
                                       
                                   }];
}



/*
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;
    NSLog(@"%f %f",newLocation.coordinate.longitude,newLocation.coordinate.latitude);
    
    [locationManager stopUpdatingLocation];
}


- (void) authentication
{
    NSError* error;
    
    SocketAuthentication *responseAuth = [[SocketAuthentication alloc]init];
    
    SocketResponseStat *responseStat = [[SocketResponseStat alloc]init];
    
    responseStat.status = true;
    responseStat.command = @"AUTHENTICATION";
    responseStat.onlyReceiverMember = false;
    
    
    SocketLogin *responseData = [[SocketLogin alloc]init];
    responseData.id = [[defaults objectForKey:@"login_id"] intValue];
    responseData.u_id = [[defaults objectForKey:@"id"] intValue];
    responseData.email = [defaults objectForKey:@"email"];
    responseData.access_token = [defaults objectForKey:@"access_token"];
    responseData.created_date = [defaults objectForKey:@"login_created_date"];
    responseData.type = [[defaults objectForKey:@"type"] intValue];
    
    Location *location = [[Location alloc]init];
    location.lat= 10.00;
    location.lon = 10.00;
    
    
    User *user = [[User alloc]init];
    user.id = [[defaults objectForKey:@"id"] intValue];
    user.f_name = [defaults objectForKey:@"f_name"];
    user.l_name = [defaults objectForKey:@"l_name"];
    user.created_date = [defaults objectForKey:@"user_created_date"];
    user.address = [defaults objectForKey:@"address"];
    
    
    responseData.user = user;
    responseData.location = location;
    
    responseAuth.responseStat = responseStat;
    responseAuth.responseData = responseData;
    
    NSLog(@"%@",self.responseAuth);
    
    
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:[responseAuth toDictionary] options:NSJSONWritingPrettyPrinted error:&error];
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
   jsonString=  [[jsonString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
   jsonString = [NSString stringWithFormat:@"%@\n",jsonString];
    NSData *reqdata = [[NSData alloc] initWithData:[jsonString dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    NSLog(@"%@", jsonString);
    
    [outputStream write:[reqdata bytes] maxLength:[reqdata length]];
    
}

- (void) getTeamMemberLocation:(Team*) team
{
    NSError* error;
    
    SocketTeamResponse *responseAuth = [[SocketTeamResponse alloc]init];
    
    
    
    SocketResponseStat *responseStat = [[SocketResponseStat alloc]init];
    
    responseStat.status = true;
    responseStat.command = @"GET_TEAM_MEMBER_LOCATION";
    responseStat.onlyReceiverMember = false;
    
    
    SocketTeamData *responseData = [[SocketTeamData alloc]init];
    
    responseData.team = team;
   
    
    responseAuth.responseStat = responseStat;
    responseAuth.responseData = responseData;
    
    NSLog(@"%@",self.responseAuth);
    
    
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:[responseAuth toDictionary] options:NSJSONWritingPrettyPrinted error:&error];
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    jsonString=  [[jsonString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
    jsonString = [NSString stringWithFormat:@"%@\n",jsonString];
    NSData *reqdata = [[NSData alloc] initWithData:[jsonString dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    NSLog(@"%@", jsonString);
    
    [outputStream write:[reqdata bytes] maxLength:[reqdata length]];
    
}

- (void) broadcastLoaction:(NSString*) json
{
    
    NSError* err = nil;
    SocketBroadcastResponse *response = [[SocketBroadcastResponse alloc] initWithString:json error:&err];
    SocketBroadcastResponse *request = [[SocketBroadcastResponse alloc]init];
    SocketBroadcastData *requestData = [[SocketBroadcastData alloc]init];
    SocketResponseStat *responseStat = [[SocketResponseStat alloc]init];
    
    NSError* error;
    
  
    
    responseStat.status = true;
    responseStat.command = @"BROADCAST_LOCATION";
    responseStat.onlyReceiverMember = false;
    
    
    SocketLogin *sender = [[SocketLogin alloc]init];
    sender.id = [[defaults objectForKey:@"login_id"] intValue];
    sender.u_id = [[defaults objectForKey:@"id"] intValue];
    sender.email = [defaults objectForKey:@"email"];
    sender.access_token = [defaults objectForKey:@"access_token"];
    sender.created_date = [defaults objectForKey:@"login_created_date"];
    sender.type = [[defaults objectForKey:@"type"] intValue];
    
    Location *location = [[Location alloc]init];
    location.lat= currentLocation.coordinate.latitude;
    location.lon = currentLocation.coordinate.longitude;
    
    
    User *user = [[User alloc]init];
    user.id = [[defaults objectForKey:@"id"] intValue];
    user.f_name = [defaults objectForKey:@"f_name"];
    user.l_name = [defaults objectForKey:@"l_name"];
    user.created_date = [defaults objectForKey:@"user_created_date"];
    user.address = [defaults objectForKey:@"address"];
    
    
    sender.user = user;
    sender.location = location;
    
    requestData.sender = sender;
    requestData.receiver = response.responseData.sender;
    
    request.responseStat = responseStat;
    request.responseData = requestData;
    
   
    
    
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:[request toDictionary] options:NSJSONWritingPrettyPrinted error:&error];
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    jsonString=  [[jsonString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
    jsonString = [NSString stringWithFormat:@"%@\n",jsonString];
    NSData *reqdata = [[NSData alloc] initWithData:[jsonString dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    NSLog(@"%@", jsonString);
    
    [outputStream write:[reqdata bytes] maxLength:[reqdata length]];
    
}

-(void) createTeamMemberLocation:(NSString*) json
{
    GMSMarker *marker =[[GMSMarker alloc] init];
    NSError* err = nil;
    SocketBroadcastResponse *response = [[SocketBroadcastResponse alloc] initWithString:json error:&err];
    NSLog(@"Create response : %@",response);
    
    if(response)
    {
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(response.responseData.sender.location.lat, response.responseData.sender.location.lat);
    marker.title = response.responseData.sender.user.f_name;
    marker.snippet = response.responseData.sender.user.l_name;
    marker.position = location;
    marker.map = self.mapView;
    
    [self.mapView setSelectedMarker:marker];
    }
}

-(void) myCurrentLocation
{
    GMSMarker *marker=[[GMSMarker alloc] init];
    marker.title = @"Me";
    marker.snippet = [defaults objectForKey:@"f_name"];
    marker.position = currentLocation.coordinate;
    marker.map = self.mapView;
    
    [self.mapView setSelectedMarker:marker];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initNetworkCommunication {
    
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"27.147.149.178", 9091, &readStream, &writeStream);
    
    inputStream = (__bridge NSInputStream *)readStream;
    outputStream = (__bridge NSOutputStream *)writeStream;
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [inputStream open];
    [outputStream open];
    
    
    
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    
    NSLog(@"stream event %lu", (unsigned long)streamEvent);
    
    switch (streamEvent) {
            
        case NSStreamEventOpenCompleted:
            NSLog(@"Stream opened");
            break;
        case NSStreamEventHasBytesAvailable:
            
            if (theStream == inputStream) {
                
                uint8_t buffer[1024];
                int len;
                
                while ([inputStream hasBytesAvailable]) {
                    len = (int)[inputStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0) {
                        
                        NSString *output = [[NSString alloc] initWithBytes:buffer length:len encoding:NSASCIIStringEncoding];
                        NSArray *outputArray = [[NSArray alloc] init];
                        
                        if (nil != output) {
                            
                            NSLog(@"Check Receive :::::::%@|||",[output substringFromIndex:[output length]-1]);
                            
                            NSLog(@"server said: %@", output);
                            
                            
                            if([[output substringFromIndex:[output length]-1] isEqualToString:@"\n"])
                            {
                                
                                self.outputS = (self.receive)?output:[NSString stringWithFormat:@"%@%@",self.outputS,output];
                                self.receive = true;
                                
                                 NSLog(@"True Current output: %@",self.outputS);
                            
                                if([[self.outputS componentsSeparatedByString:@"INCOMING_LOCATION"] count]>1)
                                {
                                    
                                    outputArray = [self.outputS componentsSeparatedByString:@"}}}"];
                                    
                                    
                                    for (int i=0;i<[outputArray count];i++) {
                                        
                                        if(i==[outputArray count]-1)
                                        {
                                        
                                            [self createTeamMemberLocation:[outputArray objectAtIndex:i]];
                                            [self myCurrentLocation];
                                            
                                        }
                                        else
                                        {
                                            [self createTeamMemberLocation:[NSString stringWithFormat:@"%@}}}",[outputArray objectAtIndex:i]]];
                                            [self myCurrentLocation];
                                            
                                        }
                                        
                                    }
                                }
                                else
                                {
                                    NSError* err = nil;
                                    SocketResponse *response = [[SocketResponse alloc] initWithString:output error:&err];
                                    NSLog(@"server said json: %@", err);
                                    NSLog(@"server said json: %@", response);
                                    
                                    if([response.responseStat.command isEqualToString:@"GIVE_LOCATION"])
                                    {
                                        [self broadcastLoaction:self.outputS];
                                    }
                                    
                                    
                                    
                                    
                                    if([response.responseStat.command isEqualToString:@"INCOMING_LOCATION"])
                                    {
                                        [self createTeamMemberLocation:self.outputS];
                                        [self myCurrentLocation];
                                    }

                                }
                            }
                            else
                            {
                                self.outputS = [NSString stringWithFormat:@"%@%@",self.outputS,output];
                                self.receive = false;
                                
                                NSLog(@"False Current output: %@",self.outputS);
                            }
                            
                           
                            
                            
                            
                            
                        }
                    }
                }
            }
            break;
            
            
        case NSStreamEventErrorOccurred:
            
            NSLog(@"Can not connect to the host!");
            break;
            
        case NSStreamEventEndEncountered:
            
            [theStream close];
            [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            theStream = nil;
            
            break;
        default:
            NSLog(@"Unknown event");
    }
    
}*/




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
