//
//  Team.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/25/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"

@protocol Team

@end


@interface Team : JSONModel

@property (assign, nonatomic) int id;
@property (strong, nonatomic) NSString  *name;
@property (strong, nonatomic) NSString  *created_date;

@end
