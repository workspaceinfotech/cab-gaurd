//
//  Location.h
//  Cabguard
//
//  Created by Workspace Infotech on 9/2/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"

@interface Location : JSONModel

@property (assign, nonatomic) double  lon;
@property (assign, nonatomic) double  lat;

@end
