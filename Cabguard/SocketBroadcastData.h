//
//  SocketBroadcastData.h
//  Cabguard
//
//  Created by Workspace Infotech on 9/4/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"
#import "SocketLogin.h"

@interface SocketBroadcastData : JSONModel

@property (strong, nonatomic) SocketLogin *sender;
@property (strong, nonatomic) SocketLogin *receiver;

@end
