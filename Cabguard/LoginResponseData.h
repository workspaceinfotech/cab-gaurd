//
//  LoginResponseData.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/24/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"
#import "UserStatus.h"
#import "Login.h"

@interface LoginResponseData : JSONModel

@property (strong, nonatomic) Login <Optional> *login;
@property (strong, nonatomic) UserStatus  <Optional> *status;

@end
