//
//  ProfileViewController.m
//  Cabguard
//
//  Created by Workspace Infotech on 8/24/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "ProfileViewController.h"
#import "JSONHTTPClient.h"
#import "ToastView.h"
#import "SocketAccess.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        
    defaults = [NSUserDefaults standardUserDefaults];
    baseurl = [defaults objectForKey:@"baseurl"];
    self.firstName.text = [defaults objectForKey:@"firstName"];
    self.lastName.text = [defaults objectForKey:@"lastName"];
    self.email.text = [defaults objectForKey:@"email"];
    
    NSString *fulladdress = [defaults objectForKey:@"address"];
    NSLog(@"%@",fulladdress);
    NSArray *split = [fulladdress componentsSeparatedByString:@"|"];
    self.address.text = split[0];
    
    if([split count]>1)
    {
    self.zipcode.text = split[1];
    }
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleTap];

}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self.email resignFirstResponder];
    [self.firstName resignFirstResponder];
    [self.lastName resignFirstResponder];
    [self.address resignFirstResponder];
     [self.zipcode resignFirstResponder];
    
}


- (IBAction)update:(id)sender {
    
    NSString *address= [NSString stringWithFormat:@"%@|%@",self.address.text,self.zipcode.text];
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@profile/change/inf",baseurl] bodyString:[NSString stringWithFormat:@"f_name=%@&l_name=%@&address=%@",self.firstName.text,self.lastName.text,address]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.data = [[ProfileResponse alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       
                                       if(self.data.responseStat.status){
                                           
                                           [defaults setValue:self.data.responseData.f_name forKey:@"firstName"];
                                           [defaults setValue:self.data.responseData.l_name forKey:@"lastName"];
                                           [defaults setValue:self.data.responseData.address forKey:@"address"];
                                           [ToastView showToastInParentView:self.view withText:@"Update successful" withDuaration:2.0];
                        
                                       }
                                       else{
                                           
                                           [ToastView showToastInParentView:self.view withText:@"Update Unsuccessful" withDuaration:2.0];
                                       }
                                       
                                       
                                   }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
