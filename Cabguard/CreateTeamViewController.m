//
//  CreateTeamViewController.m
//  Cabguard
//
//  Created by Workspace Infotech on 8/24/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "CreateTeamViewController.h"
#import "JSONHTTPClient.h"
#import "CreateTeamTableViewCell.h"
#import "ToastView.h"
#import "SocketAccess.h"

@interface CreateTeamViewController ()

@end

@implementation CreateTeamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    
    self.myObject = [[NSMutableArray alloc] init];
    defaults = [NSUserDefaults standardUserDefaults];
    baseurl = [defaults objectForKey:@"baseurl"];
    self.step = 0;
    [self populateSearch:@""];
    
   
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleTap];
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self.type resignFirstResponder];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


- (IBAction)search:(id)sender {
    
     [self populateSearch:self.type.text];
}


- (void) populateSearch: (NSString*) keyword{
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@search/user/foradd/",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@search/user/foradd/",baseurl] bodyString:[NSString stringWithFormat:@"keyword=%@",keyword]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.data = [[SearchAddResponse alloc] initWithDictionary:json error:&error];
                                       
                                       NSLog(@"%@",self.data);
                                       
                                       
                                       if(self.data.responseStat.status){
                
                                           [self.tableData reloadData];
                                       }
                                       
                                       
                                       
                                   }];
    
}



- (void) nameValidation: (NSString*) keyword{
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@team/is_available",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@team/is_available",baseurl] bodyString:[NSString stringWithFormat:@"name=%@",keyword]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                       NSLog(@"%@",err);
                                       
                                       NSError* error = nil;
                                       self.valid = [[Response alloc] initWithDictionary:json error:&error];
                                      
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       if(self.valid.responseStat.status)
                                       {
                                           self.teamName = self.type.text;
                                           self.type.text= @"";
                                           self.type.enabled = true;
                                           self.head.text = @"Add Members";
                                           self.tableData.hidden = NO;
                                           self.search.hidden = NO;
                                           [self populateSearch:@""];
                                           self.next.titleLabel.text = @"Next";
                                           self.step++;
                                       }
                                       else
                                       {
                                           [ToastView showToastInParentView:self.view withText:self.valid.responseStat.msg withDuaration:2.0];
                                       }
                                       
                                   }];
    

    
}


- (void) confirmCreate: (NSString*) name members: (NSString*) members{
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@team/add",baseurl]);
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@team/add",baseurl] bodyString:[NSString stringWithFormat:@"name=%@&members=%@",name,members]
                                   completion:^(NSDictionary *json, JSONModelError *err) {
                                       
                                     
                                       
                                       NSError* error = nil;
                                       self.create = [[CreateResponse alloc] initWithDictionary:json error:&error];
                                       
                                       if(error)
                                       {
                                           [ToastView showToastInParentView:self.view withText:@"Server Unreachable" withDuaration:2.0];
                                       }
                                       
                                       if(self.create.responseStat.status)
                                       {
                                           self.head.text = @"Chose a team name";
                                           self.tableData.hidden = true;
                                           self.type.enabled = true;
                                           self.search.hidden = true;
                
                                           self.next.titleLabel.text = @"Next";
                                           self.step=0;
                                           self.type.text = @"";
                                           [ToastView showToastInParentView:self.view withText:self.create.responseStat.msg withDuaration:2.0];
                                       }
                                       else
                                       {
                                          [ToastView showToastInParentView:self.view withText:self.create.responseStat.msg withDuaration:2.0];
                                       }
                                       
                                       
                                   }];
    
    
    
}


- (IBAction)back:(id)sender {
    
    if(self.step==0)
    {
       [self performSegueWithIdentifier:@"dashboard" sender:self];
    }
    else if(self.step==2)
    {
        self.teamName = self.type.text;
        self.type.text= @"";
        self.type.enabled = true;
        self.head.text = @"Add Members";
        self.tableData.hidden = NO;
        self.search.hidden = NO;
        [self populateSearch:@""];
        self.next.titleLabel.text = @"Next";
        self.step--;
        [self.tableData reloadData];
        NSLog(@"%@ OBJECTTTT ::::",self.myObject);
        NSLog(@"%@ DATAAAAA ::::",self.data);
    }
    else
    {
        self.type.text = self.teamName;
        self.head.text = @"Chose a team name";
        self.tableData.hidden = true;
        self.type.enabled = true;
        self.search.hidden = true;
        self.next.titleLabel.text = @"Next";
        self.step=0;

    }

    
}
- (IBAction)next:(id)sender {
    
    if(self.step==0)
    {
        [self nameValidation:self.type.text];
         [self.tableData reloadData];
        
    }
    else if (self.step==1)
    {
        self.type.text = self.teamName;
        self.head.text = @"Confirm Creation";
        self.type.enabled = false;
        self.tableData.hidden = false;
        self.search.hidden = true;
        self.next.titleLabel.text = @"Confirm";
        self.step++;
        [self.tableData reloadData];
    }
    else
    {
        NSString *members=@"[";
        
        for(int i=0;i<self.myObject.count;i++)
        {
            PasswordLogin *check = [self.myObject objectAtIndex:i];
            members = (i==self.myObject.count-1)?[members stringByAppendingString:[NSString stringWithFormat:@"%d]",check.user.id]]:[members stringByAppendingString:[NSString stringWithFormat:@"%d,",check.user.id]];
            
        }
        
        NSLog(@"%@",members);
        
        [self confirmCreate:self.teamName members:members];
    }
    
}

#pragma mark - table methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.step==1)
    {
        return self.data.responseData.count;
    }
    else
    {
        return self.myObject.count;
    }
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     CreateTeamTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if(self.step==1)
    {
       
        PasswordLogin *data = self.data.responseData[indexPath.row];
        cell.name.text = [NSString stringWithFormat:@"%@ %@",data.user.f_name,data.user.l_name];
        cell.email.text = data.email;
        
        
        cell.button.hidden = false;
        [cell.button.imageView setImage:[UIImage imageNamed:@"plus.png"]];
        [cell.buttonImage setImage:[UIImage imageNamed:@"green.png"]];
        cell.button.tag = indexPath.row;
        [cell.button addTarget:self action:@selector(addMember:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.sno.text = [NSString stringWithFormat:@"%ld",(long)indexPath.row+1];
        
        
        
        
        for(int i=0;i<self.myObject.count;i++)
        {
            PasswordLogin *check = [self.myObject objectAtIndex:i];
            
            if(check.user.id == data.user.id)
            {
                cell.button.hidden = true;
                cell.buttonImage.hidden = true;
            }
        }


    }
    else
    {
       
        PasswordLogin *data = [self.myObject objectAtIndex:indexPath.row];
        cell.name.text = [NSString stringWithFormat:@"%@ %@",data.user.f_name,data.user.l_name];
        cell.email.text = data.email;
        cell.button.hidden = false;
        cell.buttonImage.hidden = false;
        [cell.button.imageView setImage:[UIImage imageNamed:@"close.png"]];
        [cell.buttonImage setImage:[UIImage imageNamed:@"red.png"]];
        cell.button.tag = indexPath.row;
        [cell.button addTarget:self action:@selector(addMember:) forControlEvents:UIControlEventTouchUpInside];
        cell.sno.text = [NSString stringWithFormat:@"%ld",(long)indexPath.row+1];
        
        
        
    }
   
    
    return cell;
    
}


-(void)addMember:(UIButton*)sender
{
     NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    
    if (self.step == 1) {
    
        PasswordLogin *data = self.data.responseData[sender.tag];
        [self.myObject addObject:data];
        CreateTeamTableViewCell *cell = (CreateTeamTableViewCell *)[self.tableData cellForRowAtIndexPath:indexPath];
        cell.button.hidden = true;
        cell.buttonImage.hidden = true;
        
    }
    else
    {
        [self.myObject removeObjectAtIndex:sender.tag];
        [self.tableData deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationTop];
        [self.tableData reloadData];
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
