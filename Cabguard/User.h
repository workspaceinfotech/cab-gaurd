//
//  User.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/21/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"

@protocol User

@end

@interface User : JSONModel

@property (assign, nonatomic) int id;
//@property (strong, nonatomic) NSString  *email;
@property (strong, nonatomic) NSString <Optional> *f_name;
@property (strong, nonatomic) NSString <Optional> *l_name;
@property (strong, nonatomic) NSString <Optional> *address;
@property (strong, nonatomic) NSString <Optional> *created_date;

@end
