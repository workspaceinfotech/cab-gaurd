//
//  ProfileResponse.h
//  Cabguard
//
//  Created by Workspace Infotech on 8/24/15.
//  Copyright (c) 2015 Workspace Infotech. All rights reserved.
//

#import "JSONModel.h"
#import "User.h"
#import "ResponseStatus.h"

@interface ProfileResponse : JSONModel

@property (strong, nonatomic) ResponseStatus  *responseStat;
@property (strong, nonatomic) User  *responseData;

@end
